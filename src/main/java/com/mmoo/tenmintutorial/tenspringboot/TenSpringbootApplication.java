package com.mmoo.tenmintutorial.tenspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TenSpringbootApplication.class, args);
	}

}
